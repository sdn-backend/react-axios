# react + axios

## 사용 라이브러리
- axios
- styled-components

## 개발 목록
### 1. `axios` get 통신 코드

```javascript
  const [data, setData] = useState(null);
  const onclick = () => {
    axios.get('https://jsonplaceholder.typicode.com/todos/1').then(response => {
      setData(response.data);
    });
  };
  return (
    <div>
      <div>
        <button onClick={onclick}>불러오기</button>
      </div>
      {data && <textarea rows={7} value={JSON.stringify(data, null, 2)}/>}
    </div>
  );
```

### 2. `axios` 뉴스 데이터 받아오기
```javascript
const onclick = async() => {
  try{
    const response = await axios.get(
      'https://newsapi.org/v2/top-headlines?country=kr&category=science&apiKey=814efbf314644598898452d9cb2080af',
    );
    setData(response.data);
  }catch(e){
    console.log(e);
  }
};
```

### 3. 뉴스 뷰어 UI
1. NewsItem
2. NewsList

### 4. 뉴스 데이터 가져오기
```javascript
const NewsList = () => {
  const [articles, setArticles] = useState(null);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      setLoading(true);
      try { 
        const response = await axios.get(
          'https://newsapi.org/v2/top-headlines?country=kr&category=science&apiKey=814efbf314644598898452d9cb2080af',
        );
        setArticles(response.data.articles);
      }catch(e){
        console.log(e);
      }
      setLoading(false);
    };
    fetchData();
  }, []);

  // 대기 중
  if(loading) {
    return <NewsListBlock>대기 중...</NewsListBlock>
  }
  if(!articles){
    return null;
  }

  // articles 값 유효할 때
  return(
    <NewsListBlock>
      {articles.map(article => (
        <NewsItem key={article.url} article={article} />
      ))}
    </NewsListBlock>
  );
};
```
- `결과`
![](img/ex1.jpg)